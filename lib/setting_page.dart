import 'package:flutter/material.dart';
import 'package:preferences/preferences.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';

class SettingPage extends StatefulWidget{
  SettingPage() : super();
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {

  @override
  Widget build(BuildContext context) {
    LocaleHelper localeHelper = LocaleHelper(context: context);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: Icon(
          Icons.settings,
          color: Theme.of(context).hoverColor,
        ),
        title: Text(
          "الإعدادات",
          style: TextStyle(
            color: Theme.of(context).hoverColor
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).hoverColor,
        padding: EdgeInsets.only(right: 8,left: 8),
        child: PreferencePage([
          PreferenceTitle(' لغة عرض التطبيق',style: TextStyle(
            color: Theme.of(context).primaryColorDark,
            fontWeight: FontWeight.w600
          ),),
          Container(
            height: 35,
            child: RadioPreference(
              'اللغة العربية',
              'ar',
              'lang',
              isDefault: true,
            ),
          ),
          Container(
            height: 35,
            child: RadioPreference(
              'اللغة الانجليزية',
              'en',
              'lang',
            ),
          ),
          SizedBox(height: 20,),
          Divider(),
          PreferenceTitle(localeHelper.selectPlayer(),style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontWeight: FontWeight.w600
          ),),
          Container(
            height: 40,
            child: RadioPreference(
              localeHelper.lang() == 'ar'
                  ? 'محمود خليل الحصري'
                  : 'Mahmood Khaleel Al-Husary',
              'Husary_128kbps',
              'audio_player_sound',
              ),
          ),
          Container(
            height: 40,
            child: RadioPreference(
              localeHelper.lang() == 'ar' ? 'محمد صديق المنشاوي' : "Muhamad sidiyq almunshawi",
              'Minshawy_Murattal_128kbps',
              'audio_player_sound',

            ),
          ),
          SizedBox(height: 30,),
          Divider(),
          RaisedButton(
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.check),
                SizedBox(width: 10,),
                Text(localeHelper.save())
              ],
            ),
            onPressed: (){
              Navigator.of(context).pushNamed("/HomeScreen");
            },
          ),
          SizedBox(height: 10,),
        ]),
      ),
    );
  }
}