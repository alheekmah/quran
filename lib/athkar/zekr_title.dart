import 'package:alquranalkareem/athkar/zekr_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

///  ZekrTitle(zekrList[index].category));
///

class ZekrTitle extends StatelessWidget {
  //final List<ZekrObject> azkarList;
  final List<String> finalCategoryList;

  ZekrTitle(this.finalCategoryList);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          right: 8.0,
          left: 8.0
        ),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200.0,
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                childAspectRatio: 1.8,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                    alignment: Alignment.center,
                    child: InkWell(
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            left: -190,
                            top: -200,
                            child: CircleAvatar(
                              radius: 132,
                              backgroundColor: Theme.of(context).bottomAppBarColor,
                            ),
                          ),
                          Positioned(
                            left: -170,
                            top: -220,
                            child: CircleAvatar(
                              radius: 130,
                              backgroundColor: Theme.of(context).primaryColorDark,
                            ),
                          ),
                          Positioned(
                            right: -190,
                            bottom: -215,
                            child: CircleAvatar(
                              radius: 132,
                              backgroundColor: Theme.of(context).bottomAppBarColor,
                            ),
                          ),
                          Positioned(
                            right: -190,
                            bottom: -220,
                            child: CircleAvatar(
                              radius: 130,
                              backgroundColor: Theme.of(context).primaryColorDark,
                            ),
                          ),
                          Hero(
                            tag: this.finalCategoryList[index],
                            child: Material(
                              type: MaterialType.transparency,
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    finalCategoryList[index],
                                    style: TextStyle(
                                      backgroundColor: Theme.of(context).bottomAppBarColor,
                                        color: Theme.of(context).hoverColor,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600,
                                    fontFamily: 'Tajawal'),
                                    maxLines: 2,
                                    textDirection: TextDirection.rtl,
                                    softWrap: true,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            transitionOnUserGestures: true,
                          ),
                        ],
                      ),
                      onTap: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (buildContext) => ZekrList.fromFont(
                                  category: finalCategoryList[index],
                                  font: 'Tajawal',
                                )));
                      },
                    ),
                  );
                },
                childCount: 135,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
