import 'package:alquranalkareem/athkar/push_notification_test_methods.dart';
import 'package:alquranalkareem/athkar/zekr_details.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:alquranalkareem/athkar/zekr_title.dart';

import 'azkar_categories_list.dart';

import 'favourites.dart';

class Athkar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AthkarState();
  }
}

class _AthkarState extends State<Athkar> {
  final List<Map<String, String>> mappedData = mappedJson;
  final List<String> onlyAzkarList = [];
  var finalCategoryList = [];
  var _enableNotifivations = true;
  final FlutterLocalNotificationsPlugin notification =
  FlutterLocalNotificationsPlugin();
  final FlutterLocalNotificationsPlugin notification2 =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
//    var androidSettings = AndroidInitializationSettings('app_icon');
//    var IOSSettings = IOSInitializationSettings(
//        onDidReceiveLocalNotification: (id, title, body, payload) =>
//            onSelectNotification(payload));
//    notification.initialize(
//        InitializationSettings(androidSettings, IOSSettings),
//        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    notification.cancelAll();
    return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ZekrList.fromFont(
                  category: payload,
                  font: 'Tajawal',
                )));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    EnableNotifications.readFromFile().then((contents) {
      var filecontenst = contents;
      if (filecontenst == '') {
        showNotification3(
            notification: notification,
            channelId: 1,
            title: "أذكار الصباح",
            time: Time(4, 30, 0));
        showNotification3(
            notification: notification,
            channelId: 2,
            title: "أذكار المساء",
            time: Time(16, 0, 0));
        showNotification3(
            notification: notification,
            channelId: 3,
            title: "أذكار الاستيقاظ من النوم",
            time: Time(7, 0, 0));
        showNotification3(
            notification: notification,
            channelId: 4,
            title: "أذكار النوم",
            time: Time(23, 0, 0));
        debugPrint('notifications enabled');
        EnableNotifications.enable();
      } else {
        debugPrint("any habal");
      }
    });

    for (int c = 0; c < mappedData.length; c++) {
      onlyAzkarList.add(mappedData[c]["category"]);
    }
    finalCategoryList = onlyAzkarList.toSet().toList();
    debugPrint("nuber of ${finalCategoryList.length}");

    return SafeArea(

      top: true,
      bottom: true,
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Theme.of(context).primaryColor, // The Bright color of the theme
                    Theme.of(context).primaryColorDark, // The dark color of the theme
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                )),
          ),
          Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            drawerDragStartBehavior: DragStartBehavior.down,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              actions: [
                Tooltip(
                  message: 'قائمة الأذكار المفضلة',
                  child: IconButton(
                      icon: Icon(
                        Icons.stars,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (context) => FavouriteList()));
                      }),
                ),
              ],
              backgroundColor: Theme.of(context).primaryColorLight,
              title: Text(
                "قائمة الأذكار",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0,fontFamily: 'Tajawal',
                backgroundColor: Theme.of(context).bottomAppBarColor),
//                  textDirection: TextDirection.rtl,
              ),
              centerTitle: true,
              elevation: 0.0,
            ),
            drawerScrimColor: Theme.of(context).primaryColor,
            body: ZekrTitle(finalCategoryList),
          ),
        ],
      ),
    );
  }
}