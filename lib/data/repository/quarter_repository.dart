
import 'package:alquranalkareem/data/data_client.dart';
import 'package:alquranalkareem/data/model/quarter.dart';
import 'package:sqflite/sqflite.dart';

class QuarterRepository {
  DataBaseClient _client;
  QuarterRepository(){
    _client = DataBaseClient.instance;
  }

  Future<List<Quarter>> all() async {
    Database database = await _client.database;
    List<Map> results = await database.query(Quarter.tableName,columns: Quarter.columns);
    List<Quarter> quarterList =  List();
    results.forEach((result) {
      quarterList.add(Quarter.fromMap(result));
    });
    return quarterList;
  }
}