/*
* CREATE TABLE "en_ahmedali" (
	"index"	INTEGER,
	"sura"	INTEGER,
	"aya"	INTEGER,
	"text"	TEXT
)*/
class Translate{
  static String tableName = "ar_baghawy";
  static String tableName2 = "ar_ibnkathir";
  int index;
  int sorah;
  int aya;
  String text;
  String ayatext;

  static final columns = ["index", "sura",'aya','text','ayatext'];

  static fromMap(Map map) {
    Translate translate = new Translate();
    translate.aya = map["aya"];
    translate.sorah = map["sura"];
    translate.text = map["text"];
    translate.ayatext = map['ayatext'];
    return translate;
  }

}