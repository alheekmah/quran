class SoraBookmark {
  SoraBookmark();
  static String tableName = "SorahBookmark";
  int id;
  int PageNum;
  String SoraName_ar;
  String SoraName_En;

  static final columns = ["ID", "PageNum",'SoraName_ar','SoraName_En'];

  Map toMap() {
    Map map = {
      "PageNum": PageNum,
      "SoraName_ar": SoraName_ar,
      "SoraName_Er": SoraName_En,
    };

    if (id != null) {
      map["ID"] = id;
    }

    return map;
  }

  static fromMap(Map map) {
    SoraBookmark soraBookmark = new SoraBookmark();
    soraBookmark.id = map["ID"];
    soraBookmark.PageNum = map["PageNum"];
    soraBookmark.SoraName_ar = map["SoraName_ar"];
    soraBookmark.SoraName_En = map["SoraName_En"];
    return soraBookmark;
  }
}