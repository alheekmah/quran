class Quarter {
  static String tableName = "Quarter";
  int id;
  String part;
  String hezb;
  int quarter;
  int ayaId;
  static final columns = ["Id", "Part", "Hezb",'Quarter','AyaId',];

  Map toMap() {
    Map map = {
      "Name_ar": part,
      "Hezb": hezb,
      "Quarter": quarter,
      "AyaId": ayaId,
    };

    if (id != null) {
      map["Id"] = id;
    }

    return map;
  }

  static fromMap(Map map) {
    Quarter quarter = new Quarter();
    quarter.id = map["Id"];
    quarter.part = map["Part"];
    quarter.hezb = map["Hezb"];
    quarter.quarter = map["Quarter"];
    quarter.ayaId = map["AyaId"];
    return quarter;
  }

}