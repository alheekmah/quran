import 'package:alquranalkareem/list_screen/waqf_marks.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:preferences/preference_service.dart';
import 'package:provider/provider.dart';

import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/module/quran/show.dart';
import 'package:alquranalkareem/setting_page.dart';
import 'package:alquranalkareem/splash.dart';

import 'athkar/athkar.dart';
import 'athkar/favourites.dart';
import 'helper/app_localizations.dart';
import 'onboarding_screen.dart';

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Color(0xff161f07)
    ));
    // TODO: implement build
    return BotToastInit(
      child: Provider(
        create: (_) => AppDatabase(),
        child: MaterialApp(
          navigatorObservers: [BotToastNavigatorObserver()],
          debugShowCheckedModeBanner: false,
          title: "القران الكريم",
          supportedLocales: [
            Locale('ar', 'SA'),
            Locale('en', 'US'),
          ],
          // These delegates make sure that the localization data for the proper language is loaded
          localizationsDelegates: [
            // THIS CLASS WILL BE ADDED LATER
            // A class which loads the translations from JSON files
            AppLocalizations.delegate,
            // Built-in localization of basic text for Material widgets
            GlobalMaterialLocalizations.delegate,
            // Built-in localization for text direction LTR/RTL
            GlobalWidgetsLocalizations.delegate,
          ],
          // Returns a locale which will be used by the app
          localeResolutionCallback: (locale, supportedLocales) {
            // Check if the current device locale is supported
            for (var supportedLocale in supportedLocales) {
              if (supportedLocale.languageCode == PrefService.getString("lang")) {
                return supportedLocale;
              }
            }
            // If the locale of the device is not supported, use the first one
            // from the list (English, in this case).
            return supportedLocales.first;
          },
          theme: CustomTheme.of(context),
          home: SplashScreen(),
          routes: <String, WidgetBuilder>{
            '/HomeScreen': (BuildContext context) => QuranShow(initialPageNum: PrefService.getInt("start_page")),
            '/SettingPage': (BuildContext context) => SettingPage(),
            '/WaqfMarks': (BuildContext context) => WaqfMarks(),
            '/athkar': (BuildContext context) => Athkar(),
            '/favouriteList': (BuildContext context) => FavouriteList(),
            '/onboardingScreen': (BuildContext context) => OnboardingScreen()
          },
        ),
      ),
    );
  }



}

