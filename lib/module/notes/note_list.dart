import 'package:alquranalkareem/module/quran/quran_bookmarks.dart';
import 'package:alquranalkareem/module/quran/show.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';

class NoteList extends StatefulWidget {
  @override
  _NoteListState createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  AppDatabase database;
  LocaleHelper localeHelper;
  String title;
  String description;
  Note editNote;
  String _title;
  Bookmark _editBookmark;

  @override
  void initState() {
    super.initState();
    title = null;
    description = null;
    editNote = null;
    _title = null;
    _editBookmark = null;
  }

  updateBookmark() async {
    Bookmark bookmark = new Bookmark(
        id: _editBookmark.id,
        title: _title,
        pageNum: _editBookmark.pageNum,
        lastRead: _editBookmark.lastRead);
    await database.updateBookmark(bookmark);
    setState(() {
      _editBookmark = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    database = Provider.of<AppDatabase>(context);
    localeHelper = new LocaleHelper(context: context);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          localeHelper.notes(),
          style: TextStyle(
              fontFamily: "Tajawal", fontSize: 18, fontWeight: FontWeight.w400,
          backgroundColor: Theme.of(context).bottomAppBarColor),
        ),
        leading: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: InkWell(
            child: Image.asset( localeHelper.lang() == 'ar' ? 'assets/images/quran_back_right.png' : 'assets/images/quran_back_left.png',
              scale: 2,),
            onTap: (){
              Navigator.pop(context);
            },
          ),
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Text(
                localeHelper.bookmarks(),
                style: TextStyle(
                  color: Theme.of(context).hoverColor,
                  fontSize: 18,
                  fontFamily: 'cairo',
                ),
              ),
            ),
          ),
          Expanded(child: QuranBookmarks()),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Text(
                localeHelper.noteTitle(),
                style: TextStyle(
                  color: Theme.of(context).hoverColor,
                  fontSize: 18,
                  fontFamily: 'cairo',
                ),
              ),
            ),
          ),
          Expanded(child: _buildNoteList(context))
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        clipBehavior: Clip.antiAlias,
        backgroundColor: Theme.of(context).primaryColorLight,
        icon:Icon(FontAwesome.list_alt,
        color: Theme.of(context).hoverColor,),
        label: Text(localeHelper.addNewNote(),
          style: TextStyle(
            color: Theme.of(context).hoverColor,
            fontSize: 14,
            fontFamily: 'cairo',
            fontWeight: FontWeight.w500
          ),),

        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return _newNoteDialog();
              });
        },
      ),
    );
  }

  StreamBuilder<List<Note>> _buildNoteList(BuildContext context) {
    return StreamBuilder(
      stream: database.watchAllNotes(),
      builder: (context, AsyncSnapshot<List<Note>> snapshot) {
        final tasks = snapshot.data ?? List();

        return Directionality(
          textDirection: TextDirection.rtl,
          child: ListView.builder(
            itemCount: tasks.length,
            itemBuilder: (_, index) {
              final itemTask = tasks[index];
              return _buildListItem(itemTask, database);
            },
          ),
        );
      },
    );
  }

  Widget _buildListItem(Note itemNote, AppDatabase database) {
    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: localeHelper.delete(),
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => database.deleteNote(itemNote),
            )
          ],
          child: ListTile(
            leading: CircleAvatar(
              radius: 25,
              backgroundColor: Colors.transparent,
              child: Icon(
                FontAwesome.list_alt,
                color: Theme.of(context).bottomAppBarColor,
                size: 35,
              ),
            ),
            title: Text(itemNote.title,
                style: TextStyle(
                    color: Theme.of(context).primaryColorDark,
                    fontSize: 16,
                    fontFamily: 'cairo',
                    fontWeight: FontWeight.w500)),
            subtitle: Text(itemNote.description,
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor,
                    fontSize: 14,
                    fontFamily: 'cairo',
                    fontWeight: FontWeight.w400)),
            onTap: () {
              setState(() {
                editNote = itemNote;
                title = itemNote.title;
                description = itemNote.description;
              });
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return _newNoteDialog();
                  });
            },
          ),
        ),
        Divider(
          height: 5,
          endIndent: 32,
          indent: 32,
        )
      ],
    );
  }

  Widget _newNoteDialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      title: Text(
          editNote == null ? localeHelper.addNewNote() : localeHelper.edit()),
      contentPadding: EdgeInsets.all(8),
      actions: <Widget>[
        RaisedButton(
          color: Theme.of(context).primaryColorLight,
          child: Text(
            localeHelper.save(),
            style: TextStyle(color: Theme.of(context).hoverColor,),
          ),
          onPressed: () {
            if (editNote == null) {
              Note note =
                  new Note(id: null, title: title, description: description);
              database.insertNote(note);
            } else {
              Note note = new Note(
                  id: editNote.id, title: title, description: description);
              database.updateNote(note);
              setState(() {
                editNote = null;
              });
            }

            Navigator.pop(context);
          },
        )
      ],
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                height: 40,
                child: TextFormField(
                  initialValue: editNote != null ? editNote.title : null,
                  onChanged: (value) {
                    setState(() {
                      title = value.trim();
                    });
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Colors.grey)),
                      hintText: localeHelper.noteTitle(),
                      suffixIcon: const Icon(
                        FontAwesome.list_alt,
                        color: Color(0xff91a57d),
                      ),
                      hintStyle: TextStyle(height: 0.8,
                      fontSize: 14,
                      fontFamily: 'cairo')),
                )),
            SizedBox(
              height: 10,
            ),
            Container(
                child: TextFormField(
              initialValue: editNote != null ? editNote.description : null,
              textAlign: TextAlign.right,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              onChanged: (value) {
                setState(() {
                  description = value.trim();
                });
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xff91a57d))),
                  hintText: localeHelper.noteDetails(),
                  hintStyle: TextStyle(height: 0.12,
                  fontFamily: 'cairo',
                  fontSize: 14)),
            )),
          ],
        ),
      ),
      titleTextStyle: TextStyle(
        fontSize: 18,
        fontFamily: 'cairo',
        fontWeight: FontWeight.w500
      ),
    );
  }
}
