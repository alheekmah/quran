
import 'package:alquranalkareem/athkar/athkar.dart';
import 'package:alquranalkareem/athkar/favourites.dart';
import 'package:alquranalkareem/list_screen/waqf_marks.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';
import 'package:alquranalkareem/themes/themes.dart';
import 'package:responsive_grid/responsive_grid.dart';


class MenuList extends StatefulWidget {
  @override
  _MenuListState createState() => _MenuListState();
}

class _MenuListState extends State<MenuList>
    with SingleTickerProviderStateMixin {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  LocaleHelper localeHelper;
  final List<Tab> myTabs = <Tab>[
    Tab(
        child: Icon(
      Icons.widgets,
      color: Color(0xfff3efdf),
      size: 25,
    )),
    Tab(
      child: Image.asset(
        'assets/images/waqf_ic.png',
        scale: 1,
        color: Color(0xfff3efdf),
      ),
    ),
    Tab(
      child: Image.asset(
        'assets/images/azkar.png',
        scale: 10,
      ),
    ),
    Tab(
      child: Image.asset(
        'assets/images/bookmark.png',
        scale: 20,
      ),
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: myTabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    localeHelper = new LocaleHelper(context: context);
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: InkWell(
              child: Image.asset(localeHelper.lang() == 'ar' ? 'assets/images/quran_back_right.png' : 'assets/images/quran_back_left.png',
              scale: 2,),
              onTap: (){
                Navigator.pop(context);
              },
            ),
          ),
          backgroundColor: Theme.of(context).primaryColorLight,
          bottom: TabBar(controller: _tabController, tabs: myTabs),
        ),
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            _MenuList(context),
            WaqfMarks(),
            Athkar(),
            FavouriteList(),
          ],
        ),
      ),
    );
  }

  Widget _MenuList(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).hoverColor,
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ResponsiveGridRow(children: [
                    ResponsiveGridCol(
                      lg: 12,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Theme.of(context).primaryColorLight,
                            ),
                            height: 130,
                            width: 240,
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    child: Image.asset(
                                      'assets/images/waqf.png',
                                      fit: BoxFit.fitHeight,
                                      color: Theme.of(context)
                                          .hoverColor
                                          .withOpacity(0.1),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image.asset(
                                          'assets/images/waqf.png',
                                          scale: 2,
                                          color: Theme.of(context).hoverColor,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Center(
                                        child: Text(
                                          'أحكام التلاوة - Rulings of recitation',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: 'Tajawal',
                                              fontSize: 18,
                                              color:
                                                  Theme.of(context).hoverColor),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            _tabController
                                .animateTo((_tabController.index + 1) % 2);
                          },
                        ),
                      ),
                    ),
                    ResponsiveGridCol(
                      xs: 6,
                      md: 3,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Theme.of(context).primaryColorLight,
                            ),
                            height: 150,
                            width: 90,
                            child: Stack(
                              children: <Widget>[
                                Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    child: Image.asset(
                                      'assets/images/azkar.png',
                                      fit: BoxFit.fitHeight,
                                      color:
                                          Colors.transparent.withOpacity(0.1),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image.asset(
                                          'assets/images/azkar.png',
                                          scale: 7,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Center(
                                        child: Text(
                                          'حصن المسلم\nHisn Al Muslim',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: 'Tajawal',
                                              fontSize: 18,
                                              color:
                                                  Theme.of(context).hoverColor),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            _tabController
                                .animateTo((_tabController.index + 2));
                          },
                        ),
                      ),
                    ),
                    ResponsiveGridCol(
                      xs: 6,
                      md: 3,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Theme.of(context).primaryColorLight,
                            ),
                            height: 150,
                            width: 90,
                            child: Stack(
                              children: <Widget>[
                                Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    child: Image.asset(
                                      'assets/images/bookmark.png',
                                      fit: BoxFit.fitHeight,
                                      color:
                                          Colors.transparent.withOpacity(0.1),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image.asset(
                                          'assets/images/bookmark.png',
                                          scale: 7,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            _tabController
                                .animateTo((_tabController.index + 3));
                          },
                        ),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    localeHelper.themeTitle(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        backgroundColor: Theme.of(context).bottomAppBarColor,
                        color: Theme.of(context).primaryColorDark,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Tajawal'),
                  ),
                  Divider(
                    color: Theme.of(context).primaryColorDark,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 190,
                        width: 100,
                        child: Card(
                          color: Theme.of(context).primaryColorLight,
                          child: InkWell(
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image.asset(
                                          'assets/images/light.jpg',
//                                          scale: 20,
                                        ),
                                      ),
                                      Container(
                                          width: 100,
                                          height: 100,
                                          child: Icon(Icons.done,
                                              size: 70,
                                              color: Theme.of(context)
                                                          .brightness ==
                                                      Brightness.light
                                                  ? Color(0xffcdba72)
                                                  : Colors.transparent)),
                                    ],
                                  ),
                                  Container(
                                    height: 30,
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    color: Theme.of(context).primaryColorLight,
                                    child: Text(localeHelper.light(),
                                        style: TextStyle(
                                            fontFamily: 'cairo',
                                            fontSize: 16,
                                            color:
                                                Theme.of(context).hoverColor)),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              _changeTheme(context, MyThemeKeys.LIGHT);
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: 190,
                        width: 100,
                        child: Card(
                          color: Theme.of(context).primaryColorLight,
                          child: InkWell(
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image.asset(
                                          'assets/images/dark.jpg',
//                                          scale: 20,
                                        ),
                                      ),
                                      Container(
                                          width: 100,
                                          height: 100,
                                          child: Icon(Icons.done,
                                              size: 70,
                                              color: Theme.of(context)
                                                          .brightness ==
                                                      Brightness.dark
                                                  ? Color(0xffcdba72)
                                                  : Colors.transparent)),
                                    ],
                                  ),
                                  Container(
                                    height: 30,
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    color: Theme.of(context).primaryColorLight,
                                    child: Text(localeHelper.dark(),
                                        style: TextStyle(
                                            fontFamily: 'cairo',
                                            fontSize: 16,
                                            color:
                                                Theme.of(context).hoverColor)),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              _changeTheme(context, MyThemeKeys.DARK);
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
