import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';
import 'package:flutter_linkify/flutter_linkify.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LocaleHelper localeHelper = new LocaleHelper(context: context);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          localeHelper.aboutUs(),
          style: TextStyle(
              fontFamily: "Tajawal",
              fontSize: 18,
              fontWeight: FontWeight.w400,
              backgroundColor: Theme.of(context).bottomAppBarColor),
        ),
        leading: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: InkWell(
            child: Image.asset(
              localeHelper.lang() == 'ar'
                  ? 'assets/images/quran_back_right.png'
                  : 'assets/images/quran_back_left.png',
              scale: 2,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).hoverColor,
      body: SingleChildScrollView(
        child: Scrollbar(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Center(
                  child: Image.asset(
                "assets/images/zakh2.png",
                width: 280,
                color: Theme.of(context).primaryColorLight,
              )),
              SizedBox(height: 16),
              Center(
                child: Text(
                  localeHelper.stopTitle(),
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      color: Theme.of(context).primaryColorLight,
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w700),
                ),
              ),
              Divider(
                height: 5,
                endIndent: 32,
                indent: 32,
              ),
              Divider(
                height: 0,
                endIndent: 32,
                indent: 32,
              ),
              SizedBox(
                height: 32,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    localeHelper.aboutApp(),
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontSize: 18,
                        color: Theme.of(context).primaryColorLight),
                  ),
                ),
              ),
              SizedBox(
                height: 32,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  localeHelper.aboutApp2(),
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      backgroundColor: Theme.of(context).primaryColor,
                      fontFamily: 'Tajawal',
                      fontSize: 20,
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  localeHelper.aboutApp3(),
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontFamily: 'Tajawal',
                    fontSize: 18,
                    color: Theme.of(context).primaryColorLight,
                  ),
                ),
              ),
              Divider(),
              SizedBox(
                height: 20,
              ),
              Center(
                child: SelectableLinkify(
                  text: localeHelper.email(),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: Colors.blueGrey),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Image.asset(
                "assets/images/alheemah.png",
                width: 220,
                color: Theme.of(context).primaryColorLight,
              )),
            ],
          ),
        ),
      ),
    );
  }
}
