import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:preferences/preference_service.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';
import 'package:alquranalkareem/main.dart';
import 'file:///D:/alquranalkareem/lib/module/menu_list.dart';
import 'package:alquranalkareem/module/notes/note_list.dart';
import 'package:alquranalkareem/module/quran/quran_search.dart';

import '../../about_page.dart';

class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LocaleHelper localeHelper = new LocaleHelper(context: context);
    // TODO: implement build
    return Material(
      elevation: 8,
      child: Container(
        height: 122,
        decoration: BoxDecoration(color: Theme.of(context).primaryColorDark,
          border:
          Border.all(width: 1.5, color: Theme.of(context).primaryColor),
            ),
        padding: EdgeInsets.only(top: 5),
        //color: Colors.blueGrey[800],
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 50,
              padding: EdgeInsets.only(top: 7),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: ((context) => QuranSearch())));
                      },
                      child: Card(
                        color: Theme.of(context).backgroundColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: Center(
                                    child: Text(
                                  localeHelper.searchHint(),
                                  style: TextStyle(
                                      fontFamily: "cairo",
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14,
                                      color: Theme.of(context).textSelectionColor.withOpacity(0.5)),
                                )),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                                child: Icon(
                                  FontAwesome.search,
                                  size: 18,
                                  color: Theme.of(context).textSelectionColor.withOpacity(0.5),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
//                  IconButton(
//                    icon: Icon(
//                      Icons.notifications_active,
//                      color: Theme.of(context).hoverColor,
//                    ),
//                    onPressed: () {
//                      DatePicker.showTimePicker(context,
//                          locale: LocaleType.ar,
//                          showTitleActions: true, onConfirm: (time) {
//                        print("Time ${time.hour}:${time.minute}");
//                      });
//                    },
//                  ),
                  IconButton(
                    icon: Icon(
                      FontAwesome.language,
                      color: Theme.of(context).hoverColor,
                    ),
                    onPressed: () {
                      PrefService.setString(
                          "lang", localeHelper.lang() == "en" ? "ar" : "en");
                      RestartWidget.restartApp(context);
                    },
                  ),
                ],
              ),
            ),
            Divider(
              height: 12,
            ),
            Container(
              height: 48,
              //padding: EdgeInsets.only(top: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _btnIcon(
                      localeHelper.menu(), Icons.widgets, MenuList()),
                  VerticalDivider(
                    width: 2,
                  ),
                  _btnIcon(localeHelper.notes(), MaterialIcons.book, NoteList()),
                  VerticalDivider(
                    width: 2,
                  ),
                  _btnIcon(localeHelper.aboutUs(), Icons.info, AboutPage()),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _btnIcon(String title, IconData iconData, Widget page) {
    return RaisedButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: ((context) => page)));
      },
      color: Colors.transparent,
      elevation: 0,
      textColor: Theme.of(context).hoverColor,
      child: Column(
        children: <Widget>[
          Icon(iconData),
          Text(
            title,
            style: TextStyle(fontSize: 12, fontFamily: 'cairo', fontWeight: FontWeight.w400),
          )
        ],
      ),
    );
  }
}
