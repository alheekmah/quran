import 'dart:async';
import 'dart:io';

import 'package:alquranalkareem/data/model/aya.dart';
import 'package:alquranalkareem/data/model/sorah_bookmark.dart';
import 'package:alquranalkareem/data/repository/aya_repository.dart';
import 'package:alquranalkareem/data/repository/sorah_bookmark_repository.dart';
import 'package:alquranalkareem/data/repository/translate2_repository.dart';
import 'package:alquranalkareem/event/font_size_event.dart';
import 'package:alquranalkareem/helper/my_event_bus.dart';
import 'package:alquranalkareem/helper/settings_helpers.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:preferences/preferences.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/data/model/ayat.dart';
import 'package:alquranalkareem/data/model/sorah.dart';
import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/data/repository/sorah_repository.dart';
import 'package:alquranalkareem/data/repository/translate_repository.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';
import 'package:alquranalkareem/module/quran/quran_bookmarks.dart';
import 'package:alquranalkareem/module/quran/sora_list.dart';
import 'package:sliding_sheet/sliding_sheet.dart';
import 'package:wakelock/wakelock.dart';
import 'package:alquranalkareem/data/model/quarter.dart';
import 'widgets/top_bar.dart';

final String assetName = 'assets/images/aya.png';

class QuranShow extends StatefulWidget {
  int initialPageNum;
  Bookmark bookmark;
  int sorahNum;

  QuranShow({Key key, this.initialPageNum, this.bookmark, this.sorahNum})
      : super(key: key);

  @override
  _QuranShowState createState() => _QuranShowState();
}

class _QuranShowState extends State<QuranShow>
    with AutomaticKeepAliveClientMixin<QuranShow>, WidgetsBindingObserver {
  static const double maxFontSizeArabic = 30;
  double fontSizeArabic = SettingsHelpers.minFontSizeArabic;
  MyEventBus _myEventBus = MyEventBus.instance;

  StreamSubscription streamEvent;

  SorahRepository sorahRepository = new SorahRepository();
  AyaRepository ayaRepository = new AyaRepository();
  SorahBookmarkRepository sorahBookmarkRepository =
      new SorahBookmarkRepository();
  List<SoraBookmark> soraBookmarkList;
  List<Sorah> sorahList;
  List<Aya> ayaList;
  LocaleHelper localeHelper;
  AppDatabase database;
  PageController _pageController;
  Duration duration;
  Duration position;
  AudioPlayer audioPlayer = new AudioPlayer();
  bool isPlay;
  String currentPlay;
  int currentPage;
  int currentIndex;
  bool isShowControl;
  double sliderValue;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  bool downloading = false;
  String progressString = "0%";
  double progress = 0;
  bool autoPlay = false;
  String translate = '';
  String value;
  double _height = 600;
  double _width = 400;
  String _title;
  Bookmark _editBookmark;
  double _newBookmark;

  void initAudioPlayer() {
    _positionSubscription =
        audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              position = p;
              //print("posiotn ${p.inMilliseconds}");
            }));
    _durationSubscription = audioPlayer.onDurationChanged.listen((Duration d) {
      if (duration == null) {
        //print("posiotn ${d.inMilliseconds}");
        setState(() => duration = d);
      }
    });
  }

  Future playFile(String url, String fileName) async {
    var path;
    int result;
    try {
      var dir = await getApplicationDocumentsDirectory();
      path = join(dir.path, fileName);
      var file = File(path);
      bool exists = await file.exists();
      if (!exists) {
        try {
          await Directory(dirname(path)).create(recursive: true);
        } catch (e) {
          print(e);
        }
        await downloadFile(path, url, fileName);
      }
      result = await audioPlayer.play(path, isLocal: true);
      if (result == 1) {
        setState(() {
          isPlay = true;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future downloadFile(String path, String url, String fileName) async {
    Dio dio = Dio();
    try {
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (e) {
        print(e);
      }
      setState(() {
        downloading = true;
        progressString = "0%";
        progress = 0;
      });
      await dio.download(url, path, onReceiveProgress: (rec, total) {
        //print("Rec: $rec , Total: $total");
        setState(() {
          progressString = ((rec / total) * 100).toStringAsFixed(0) + "%";
          progress = (rec / total).toDouble();
        });
      });
    } catch (e) {
      print(e);
    }

    setState(() {
      downloading = false;
      progressString = "100%";
    });
    //print("Download completed");
  }

  showControl() {
    setState(() {
      isShowControl = !isShowControl;
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    streamEvent = _myEventBus.eventBus.on<FontSizeEvent>().listen((onData) {
      if (streamEvent != null) {
        setState(() {
          fontSizeArabic = onData.arabicFontSize;
        });
      }
    });
    super.initState();
    initAudioPlayer();
    isPlay = false;
    currentPlay = null;
    isShowControl = false;
    currentPage = widget.initialPageNum;
    currentIndex = widget.initialPageNum - 1;
    _pageController = PageController(initialPage: widget.initialPageNum - 1);
    sliderValue = 0;
    getList();
    _title = null;
    _editBookmark = null;
    _newBookmark = null;
  }

  _replay(BuildContext context) {
    Navigator.pop(context);
    setState(() {
      isPlay = false;
      currentPlay = null;
    });
    if (widget.sorahNum != null) {
      playSorah();
    } else {
      play(currentPage.toString());
    }
  }

  @override
  void deactivate() {
    _positionSubscription.cancel();
    _durationSubscription.cancel();
    if (isPlay) {
      audioPlayer.pause();
    }
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _pageController.dispose();
    audioPlayer.dispose();
    streamEvent?.cancel();
    streamEvent = null;
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      if (isPlay) {
        audioPlayer.pause();
        setState(() {
          isPlay = false;
        });
      }
    }
    //print('state = $state');
  }

  saveLastPlace() async {
    PrefService.setInt("start_page", currentPage);
//    if (widget.bookmark != null) {
//      await saveBookmark();
//    }
  }

  newBookmark() async {
    var now = new DateTime.now();
    String lastRead = "${now.year}-${now.month}-${now.day}";
    Bookmark bookmark = new Bookmark(
        id: null, title: _title, pageNum: currentPage, lastRead: lastRead);
    await database.insertBookmark(bookmark);
  }

  saveBookmark2() async {
    var now = new DateTime.now();
    String lastRead = "${now.year}-${now.month}-${now.day}";
    Bookmark bookmark = new Bookmark(
        id: widget.bookmark.id,
        title: widget.bookmark.title,
        pageNum: currentPage,
        lastRead: lastRead);
    await database.updateBookmark(bookmark);
  }

  saveBookmark() async {
    if (_editBookmark == null) {
      newBookmark();
    } else {
      updateBookmark();
    }
  }

  updateBookmark() async {
    Bookmark bookmark = new Bookmark(
        id: _editBookmark.id,
        title: _title,
        pageNum: _editBookmark.pageNum,
        lastRead: _editBookmark.lastRead);
    await database.updateBookmark(bookmark);
    setState(() {
      _editBookmark = null;
    });
  }

  playSorah() async {
    String url;
    String sorahName = "${widget.sorahNum}";
    String fileName;
    if (sorahName.length == 1) {
      sorahName = "00$sorahName";
    } else if (sorahName.length == 2) {
      sorahName = "0$sorahName";
    }
    switch (PrefService.getString('audio_player_sound')) {
      case "Husary_128kbps":
        {
          fileName = "mahmood_khaleel_al-husaree/$sorahName.mp3";
          url = "http://download.quranicaudio.com/quran/$fileName";
        }
        break;
      case "Minshawy_Murattal_128kbps":
        {
          fileName = "muhammad_siddeeq_al-minshaawee/$sorahName.mp3";
          url = "http://download.quranicaudio.com/quran/$fileName";
        }
        break;
    }

    //print("url $url");
    if (isPlay) {
      audioPlayer.pause();
      setState(() {
        isPlay = false;
      });
    } else {
      await playFile(url, fileName);
    }
  }

  play(String page) async {
    if (isPlay) {
      audioPlayer.pause();
      setState(() {
        isPlay = false;
      });
    } else {
      int result;
      if (currentPlay == page) {
        result = await audioPlayer.resume();
        if (result == 1) {
          setState(() {
            isPlay = true;
          });
        }
      } else {
        currentPlay = page;
        String fileName = page;
        if (page.length == 1) {
          fileName = "00$fileName";
        } else if (page.length == 2) {
          fileName = "0$fileName";
        }
        fileName =
            "${PrefService.getString('audio_player_sound')}/PageMp3s/Page$fileName.mp3";
        String url = "http://everyayah.com/data/$fileName";
        await playFile(url, fileName);
      }
    }
    setState(() {
      currentPlay = page;
    });
    audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        isPlay = false;
        currentPlay = null;
        position = null;
        duration = null;
        autoPlay = true;
      });
      if (widget.sorahNum == null) {
        //_pageController.nextPage(duration: Duration(milliseconds: 100), curve: Curves.ease);
        _pageController.jumpToPage(currentIndex + 1);
      }
    });
  }

  pageChanged(int index) {
    if (isPlay) audioPlayer.stop();
    print("on Page Changed $index");
    setState(() {
      isPlay = false;
      currentPlay = null;
      currentPage = index + 1;
      currentIndex = index;
      position = null;
      duration = null;
      isShowControl = false;
    });
    if (autoPlay) {
      play(currentPage.toString());
      print("current page $currentPage");
      setState(() {
        autoPlay = false;
      });
    }
    saveLastPlace();
  }

  @override
  Widget build(BuildContext context) {
    database = Provider.of<AppDatabase>(context);
    localeHelper = new LocaleHelper(context: context);
    super.build(context);
    Wakelock.enable();
    return WillPopScope(
      onWillPop: () async {
        audioPlayer.stop();
        setState(() {
          isPlay = false;
          currentPlay = null;
        });
        Wakelock.disable();
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          body: OrientationBuilder(
            builder: (context, orientation) {
              return Container(
                child: Stack(
                  children: <Widget>[
                    Directionality(
                        textDirection: TextDirection.rtl,
                        child: _quranPages(context, orientation)),
                    Visibility(visible: isShowControl, child: TopBar()),
                    _downloadingBar(context),
                    _bottomBar(context),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _quranPages(BuildContext context, Orientation orientation) {
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Center(
        child: Container(
          height: _height,
          width: _width,
          child: PageView.builder(
              controller: _pageController ??
                  PageController(initialPage: widget.initialPageNum - 1),
              itemCount: 604,
              onPageChanged: (page) {
                print("page changed $page");
                pageChanged(page);
              },
              itemBuilder: (_, index) {
                return SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: (index % 2 == 0
                      ? Container(
                          decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(12),
                                  bottomRight: Radius.circular(12))),
                          child: InkWell(
                            onLongPress: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return _newBookmarkDialog(context, index);
                                  });
//                              showDialog(
//                                context: context,
//                                builder: (context) {
//                                  return AlertDialog(
//                                    backgroundColor: Theme.of(context).primaryColorLight,
//                                    shape: RoundedRectangleBorder(
//                                      borderRadius: BorderRadius.all(
//                                        Radius.circular(8)
//                                        ),
//                                      side: BorderSide(
//                                        color: Theme.of(context).primaryColor,
//                                        style: BorderStyle.solid,
//                                      ),
//                                    ),
//                                    contentPadding: EdgeInsets.symmetric(
//                                      horizontal: 10,
//                                      vertical: 5,
//                                    ),
//                                    content: Row(
//                                      mainAxisAlignment: MainAxisAlignment.center,
//                                      mainAxisSize: MainAxisSize.min,
//                                      children: <Widget>[
//                                        Text(
//                                          '     ${localeHelper.page()}     ',
//                                          style: TextStyle(
//                                              color: Theme.of(context).hoverColor,
//                                              fontFamily: 'cairo',
//                                              fontSize: 14
//                                          ),),
//                                        Container(
//                                            height: 25,
//                                            child: VerticalDivider(
//                                              thickness: 1,
//                                            )),
//                                        Container(
//                                          width: 60,
//                                          height: 35,
//                                          child: FlatButton(
//                                            onPressed: () {
//                                              saveBookmark();
//                                              Navigator.pop(context);
//                                            },
//                                            child: Icon(
//                                              Icons.bookmark,
//                                              color: Theme.of(context).accentColor,
//                                            ),
//                                          ),
//                                        ),
//                                      ],
//                                    ),
//                                  );
//                                },
//                              );
                            },
                            onDoubleTap: () {
                              _height = _height == 600 ? 900 : 600;
                              _width = _width == 600 ? 600 : 600;
                              setState(() {});
                            },
                            onTap: () => showControl(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 48.0),
                              child: Stack(
                                children: <Widget>[
                                  Image.asset(
                                    "assets/pages/00${index + 1}.png",
                                    fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
                                color: Theme.of(context).brightness == Brightness.light
                                    ? null
                                    : Colors.white,
                                    height: orientation == Orientation.portrait
                                        ? MediaQuery.of(context).size.height - 160
                                        : null,
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                  ),
                                  Image.asset(
                                    "assets/pages/000${index + 1}.png",
                                    fit: BoxFit.fitHeight,
                                    height: orientation == Orientation.portrait
                                        ? MediaQuery.of(context).size.height - 160
                                        : null,
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  bottomLeft: Radius.circular(12))),
                          child: InkWell(
                            onLongPress: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return _newBookmarkDialog(context, index);
                                  });
                            },
                            onDoubleTap: () {
                              _height = _height == 600 ? 900 : 600;
                              _width = _width == 600 ? 600 : 600;
                              setState(() {});
                            },
                            onTap: () => showControl(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 48.0),
                              child: Stack(
                                children: <Widget>[
                                  Image.asset(
                                    "assets/pages/00${index + 1}.png",
                                    fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
                                color: Theme.of(context).brightness == Brightness.light
                                    ? null
                                    : Colors.white,
                                    height: orientation == Orientation.portrait
                                        ? MediaQuery.of(context).size.height - 160
                                        : null,
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                  ),
                                  Image.asset(
                                    "assets/pages/000${index + 1}.png",
                                    fit: BoxFit.fitHeight,
                                    height: orientation == Orientation.portrait
                                        ? MediaQuery.of(context).size.height - 160
                                        : null,
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )),
                );
              }),
        ),
      );
    } else {
      return Container(
        child: PageView.builder(
            controller: _pageController ??
                PageController(initialPage: widget.initialPageNum - 1),
            itemCount: 604,
            onPageChanged: (page) {
              print("page changed $page");
              pageChanged(page);
            },
            itemBuilder: (_, index) {
              return SingleChildScrollView(
//                scrollDirection: Axis.horizontal,
                child: (index % 2 == 0
                    ? Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(12),
                                bottomRight: Radius.circular(12))),
                        child: InkWell(
                          onLongPress: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return _newBookmarkDialog(context, index);
                                });
                          },
                          onTap: () => showControl(),
                          child: Stack(
                            children: <Widget>[
                              Image.asset(
                                "assets/pages/00${index + 1}.png",
                                fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
                                color: Theme.of(context).brightness == Brightness.light
                                    ? null
                                    : Colors.white,
                                height: orientation == Orientation.portrait
                                    ? MediaQuery.of(context).size.height - 160
                                    : null,
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                              ),
                              Image.asset(
                                "assets/pages/000${index + 1}.png",
                                fit: BoxFit.contain,
                                height: orientation == Orientation.portrait
                                    ? MediaQuery.of(context).size.height - 160
                                    : null,
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                              ),
                            ],
                          ),
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12),
                                bottomLeft: Radius.circular(12))),
                        child: InkWell(
                          onLongPress: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return _newBookmarkDialog(context, index);
                                });
                          },
                          onTap: () => showControl(),
                          child: Stack(
                            children: <Widget>[
                              Image.asset(
                                "assets/pages/00${index + 1}.png",
                                fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
                                color: Theme.of(context).brightness == Brightness.light
                                    ? null
                                    : Colors.white,
                                height: orientation == Orientation.portrait
                                    ? MediaQuery.of(context).size.height - 160
                                    : null,
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                              ),
                              Image.asset(
                                "assets/pages/000${index + 1}.png",
                                fit: BoxFit.fitHeight,
                                height: orientation == Orientation.portrait
                                    ? MediaQuery.of(context).size.height - 160
                                    : null,
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                              ),
                            ],
                          ),
                        ),
                      )),
              );
            }),
      );
    }
  }

  Widget _downloadingBar(context) {
    return Visibility(
      visible: downloading,
      child: Align(
        alignment: Alignment.center,
        child: Card(
          color: Theme.of(context).primaryColorDark,
          elevation: 20,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          child: Container(
            padding: EdgeInsets.only(top: 8, right: 10, bottom: 8, left: 10),
            child: CircularPercentIndicator(
              radius: 60.0,
              lineWidth: 5.0,
              percent: progress,
              center: new Text(
                progressString,
                style: TextStyle(color: Theme.of(context).hoverColor),
              ),
              progressColor: Theme.of(context).hoverColor,
              backgroundColor: Theme.of(context).bottomAppBarColor,
            ),
          ),
        ),
      ),
    );
  }

  Widget _bottomBar(BuildContext context) {
    super.build(context);
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Visibility(
        visible: isShowControl,
        child: _slidingSheetPortrait(context),
      );
    } else {
      return Visibility(
        visible: isShowControl,
        child: _slidingSheetlandscape(context),
      );
    }
  }

  Widget _slidingSheetPortrait(BuildContext context) {
    super.build(context);
    LocaleHelper localeHelper = new LocaleHelper(context: context);
    return Container(
      child: SlidingSheet(
        scrollSpec: ScrollSpec.bouncingScroll(),
        color: Theme.of(context).primaryColorDark,
        border:
            Border.all(width: 1.5, color: Theme.of(context).primaryColor),
        elevation: 8,
        cornerRadius: 12,
        snapSpec: const SnapSpec(
          // Enable snapping. This is true by default.
          snap: true,
          // Set custom snapping points.
          snappings: [90, 150, double.infinity],
          positioning: SnapPositioning.pixelOffset,
        ),
        builder: (context, state) {
          // This is the content of the sheet that will get
          // scrolled, if the content is bigger than the available
          // height of the sheet.
          return Container(
            height: MediaQuery.of(context).size.height / 1/2,
            child: _showTafseer(currentPage),
          );
        },
        headerBuilder: (context, state) {
          return Container(
            height: 100,
            width: double.infinity,
            color: Theme.of(context).primaryColorDark,
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.drag_handle,
                  color: Theme.of(context).hoverColor,
                  size: 30,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(12),
//                    ),
//                color: Theme.of(context).primaryColorLight,
                    elevation: 20,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor),
                      child: Column(
                        children: <Widget>[
                          Directionality(
                            textDirection: TextDirection.ltr,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 50,
                                  child: FlatButton(
                                    //color: Colors.blueGrey[900],
                                    child: Icon(
                                      isPlay
                                          ? Icons.pause_circle_outline
                                          : Icons.play_circle_outline,
                                      color: Theme.of(context).hoverColor,
                                      size: 30,
                                    ),
                                    onPressed: () {
                                      if (widget.sorahNum != null) {
                                        playSorah();
                                      } else {
                                        play("$currentPage");
                                      }
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: 50,
                                    child: SliderTheme(
                                      data: SliderThemeData(
                                          thumbShape: RoundSliderThumbShape(
                                              enabledThumbRadius: 8)),
                                      child: Slider(
                                        activeColor: Theme.of(context).hoverColor,
                                        inactiveColor:
                                            Theme.of(context).primaryColorDark,
                                        min: 0,
                                        max: duration != null
                                            ? duration.inMilliseconds.toDouble()
                                            : 0.0,
                                        value: position != null
                                            ? (position.inMilliseconds.toDouble())
                                            : 0.0,
                                        onChanged: (value) {
                                          audioPlayer.seek(new Duration(
                                              milliseconds: value.toInt()));
                                          setState(() {
                                            sliderValue = value.toDouble();
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                IconButton(
                                  padding: EdgeInsets.only(right: 25),
                                  icon: Icon(
                                    Icons.person,
                                    color: Theme.of(context).hoverColor,
                                  ),
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return _dialogOptions(context);
                                        });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        footerBuilder: (context, state) {
          return Container(
            height: 56,
            width: double.infinity,
            color: Theme.of(context).primaryColorDark,
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: ((context) => SorahList())));
                        },
                        child: Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 1.0,
                                    color: Theme.of(context)
                                        .hoverColor
                                        .withOpacity(0.5)),
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Theme.of(context).primaryColorDark,
                                      Theme.of(context).primaryColorLight
                                    ])),
                            padding: EdgeInsets.only(top: 3, bottom: 3),
                            child: Text(
                              localeHelper.quranSorah(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Theme.of(context).hoverColor,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )),
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 8)),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 1 / 8,
                        width: MediaQuery.of(context).size.width * 0.70,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Theme.of(context).primaryColor),
                        child: sorahList != null
                            ? Swiper(
                                scrollDirection: Axis.horizontal,
                                control: new SwiperControl(),
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemCount: sorahList.length,
                                itemBuilder: (BuildContext context, index) {
                                  Sorah sorah = sorahList[index];
                                  return Center(
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: ((context) =>
                                                    QuranShow(
                                                      initialPageNum:
                                                          sorah.pageNum,
                                                    ))));
                                      },
                                      child: Text(
                                        '${localeHelper.lang() == 'en' ? sorah.nameEn : sorah.name}',
                                        style: TextStyle(
                                            fontFamily: "naskh",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 22,
                                            color:
                                                Theme.of(context).accentColor),
                                      ),
                                    ),
                                  );
                                })
                            : Text(""),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _slidingSheetlandscape(BuildContext context) {
    super.build(context);
    LocaleHelper localeHelper = new LocaleHelper(context: context);
    return Container(
      width: MediaQuery.of(context).size.width / 1 / 2,
      child: SlidingSheet(
        scrollSpec: ScrollSpec.overscroll(),
        color: Theme.of(context).primaryColorDark,
        border:
            Border.all(width: 1.5, color: Theme.of(context).primaryColor),
        elevation: 8,
        cornerRadius: 12,
        snapSpec: const SnapSpec(
          // Enable snapping. This is true by default.
          snap: true,
          // Set custom snapping points.
          snappings: [40, 150, double.infinity],
          positioning: SnapPositioning.pixelOffset,
        ),
        builder: (context, state) {
          // This is the content of the sheet that will get
          // scrolled, if the content is bigger than the available
          // height of the sheet.
          return Container(
            height: MediaQuery.of(context).size.height / 1/2,
            child: _showTafseer(currentPage),
          );
        },
        headerBuilder: (context, state) {
          return Container(
            height: 100,
            width: double.infinity,
            color: Theme.of(context).primaryColorDark,
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.drag_handle,
                  color: Theme.of(context).hoverColor,
                  size: 30,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(12),
//                    ),
//                color: Theme.of(context).primaryColorLight,
                    elevation: 20,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor),
                      child: Column(
                        children: <Widget>[
                          Directionality(
                            textDirection: TextDirection.ltr,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 50,
                                  child: FlatButton(
                                    //color: Colors.blueGrey[900],
                                    child: Icon(
                                      isPlay
                                          ? Icons.pause_circle_outline
                                          : Icons.play_circle_outline,
                                      color: Theme.of(context).hoverColor,
                                      size: 30,
                                    ),
                                    onPressed: () {
                                      if (widget.sorahNum != null) {
                                        playSorah();
                                      } else {
                                        play("$currentPage");
                                      }
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: 50,
                                    child: SliderTheme(
                                      data: SliderThemeData(
                                          thumbShape: RoundSliderThumbShape(
                                              enabledThumbRadius: 8)),
                                      child: Slider(
                                        activeColor: Theme.of(context).hoverColor,
                                        inactiveColor:
                                            Theme.of(context).primaryColorDark,
                                        min: 0,
                                        max: duration != null
                                            ? duration.inMilliseconds.toDouble()
                                            : 0.0,
                                        value: position != null
                                            ? (position.inMilliseconds.toDouble())
                                            : 0.0,
                                        onChanged: (value) {
                                          audioPlayer.seek(new Duration(
                                              milliseconds: value.toInt()));
                                          setState(() {
                                            sliderValue = value.toDouble();
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                IconButton(
                                  padding: EdgeInsets.only(right: 25),
                                  icon: Icon(
                                    Icons.person,
                                    color: Theme.of(context).hoverColor,
                                  ),
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return _dialogOptions(context);
                                        });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        footerBuilder: (context, state) {
          return Container(
            height: 56,
            width: double.infinity,
            color: Theme.of(context).primaryColorDark,
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: ((context) => SorahList())));
                        },
                        child: Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 1.0,
                                    color: Theme.of(context).hoverColor),
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Theme.of(context).primaryColorDark,
                                      Theme.of(context).primaryColorLight
                                    ])),
                            padding: EdgeInsets.only(top: 3, bottom: 3),
                            child: Text(
                              localeHelper.quranSorah(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Theme.of(context).hoverColor,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )),
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 8)),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 1 / 8,
                        width: MediaQuery.of(context).size.width * 0.70,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Theme.of(context).primaryColor),
                        child: sorahList != null
                            ? Swiper(
                                scrollDirection: Axis.horizontal,
                                control: new SwiperControl(),
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemCount: sorahList.length,
                                itemBuilder: (BuildContext context, index) {
                                  Sorah sorah = sorahList[index];
                                  return Center(
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: ((context) =>
                                                    QuranShow(
                                                      initialPageNum:
                                                          sorah.pageNum,
                                                    ))));
                                      },
                                      child: Text(
                                        '${localeHelper.lang() == 'en' ? sorah.nameEn : sorah.name}',
                                        style: TextStyle(
                                            fontFamily: "naskh",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 22,
                                            color:
                                                Theme.of(context).accentColor),
                                      ),
                                    ),
                                  );
                                })
                            : Text(""),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  getList() async {
    sorahRepository.all().then((values) {
      setState(() {
        sorahList = values;
      });
    });
    sorahBookmarkRepository.all().then((values) {
      setState(() {
        soraBookmarkList = values;
      });
    });
  }

  Widget _dialogOptions(BuildContext context) {
    return SimpleDialog(
      title: Container(
          height: 40,
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColorDark,
              borderRadius: BorderRadius.all(Radius.circular(4))),
          child: Center(
            child: Text(
              localeHelper.selectPlayer(),
              style: TextStyle(
                  color: Theme.of(context).hoverColor,
                  fontFamily: 'cairo',
                  fontWeight: FontWeight.w500,
                  fontSize: 18),
            ),
          )),
      children: <Widget>[
        Divider(
          height: 2,
          indent: 24,
          endIndent: 24,
          thickness: 2,
          color: Theme.of(context).bottomAppBarColor,
        ),
        RadioPreference(
          localeHelper.lang() == 'ar'
              ? 'محمود خليل الحصري'
              : 'Mahmood Khaleel Al-Husary',
          "Husary_128kbps",
          'audio_player_sound',
          onSelect: () {
            _replay(context);
          },
        ),
        Divider(
          height: 2,
        ),
        RadioPreference(
          localeHelper.lang() == 'ar'
              ? 'محمد صديق المنشاوي'
              : "Muhamad sidiyq almunshawi",
          'Minshawy_Murattal_128kbps',
          'audio_player_sound',
          onSelect: () {
            _replay(context);
          },
        ),
        Divider(
          height: 2,
          thickness: 2,
          color: Theme.of(context).bottomAppBarColor,
        ),
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

//  void onTap() {}

  Widget _ayaList(int pageNum) {
    TranslateRepository translateRepository = new TranslateRepository();
    return FutureBuilder<List<Ayat>>(
        future: translateRepository.getPageTranslate(pageNum),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            List<Ayat> ayat = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text('تفسير البغوي',
                style: TextStyle(
                  fontFamily: 'Tajawal',
                  fontSize: 14,
                  color: Theme.of(context).accentColor
                ),),
                Divider(
                  height: 12,
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).hoverColor,
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    height: 50,
                    child: Center(
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        scrollDirection: Axis.horizontal,
                        itemCount: ayat.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, position) {
                          Ayat aya = ayat[position];
//                        Link link = ayat.length;

//                        if(index < 10){
//                          return SizedBox();
//                        }

                          return Container(
                            height: 50,
                            width: 50,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  translate =
                                      '﴿${aya.ayatext}﴾\n\n${aya.translate}';
                                });
                              },
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      'assets/images/aya3.png',
                                      width: 40,
                                      height: 40,
                                      color: Theme.of(context).brightness ==
                                              Brightness.light
                                          ? null
                                          : Theme.of(context).primaryColorLight,
                                    ),
                                  ),
                                  Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 3),
                                      child: Text(
                                        "${aya.ayaNum}",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Theme.of(context).primaryColor,
                                            fontFamily: 'naskh',
                                            fontWeight: FontWeight.w700,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _ayaList2(int pageNum) {
    TranslateRepository2 translateRepository2 = new TranslateRepository2();
    return FutureBuilder<List<Ayat>>(
        future: translateRepository2.getPageTranslate(pageNum),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            List<Ayat> ayat = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 8.0,
                ),
                Text('تفسير ابن كثير',
                  style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontSize: 14,
                      color: Theme.of(context).accentColor
                  ),),
                Divider(
                  height: 12,
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).hoverColor,
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    height: 50,
                    child: Center(
                      child: ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        scrollDirection: Axis.horizontal,
                        itemCount: ayat.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, position) {
                          Ayat aya = ayat[position];
//                        Link link = ayat.length;

//                        if(index < 10){
//                          return SizedBox();
//                        }

                          return Container(
                            height: 50,
                            width: 50,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  translate =
                                  '﴿${aya.ayatext}﴾\n\n${aya.translate}';
                                });
                              },
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      'assets/images/aya3.png',
                                      width: 40,
                                      height: 40,
                                      color: Theme.of(context).brightness ==
                                          Brightness.light
                                          ? null
                                          : Theme.of(context).primaryColorLight,
                                    ),
                                  ),
                                  Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 3),
                                      child: Text(
                                        "${aya.ayaNum}",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Theme.of(context).primaryColor,
                                            fontFamily: 'naskh',
                                            fontWeight: FontWeight.w700,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _showTafseer(int pageNum) {
    return FutureBuilder<List<Ayat>>(
      builder: (context, snapshot) {
        return Container(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              children: <Widget>[

//                _changeTafseer(),
//                _ayaList(currentPage),
//                _ayaList2(currentPage),
//                _changeTafseer(),
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).hoverColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)
                      )),
                  child: _changeWidget(pageNum),
                ),
                Flexible(
                  flex: 4,
                  child: Container(
                    height: MediaQuery.of(context).size.height / 1/2 * 1.3,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.of(context).primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height,
                        ),
                        child: Scrollbar(
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                SelectableLinkify(
                                  toolbarOptions: ToolbarOptions(
                                      copy: true, selectAll: true),
                                  text: "$translate",
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      color: Theme.of(context).hoverColor,
                                      fontFamily: 'naskh',
                                      fontWeight: FontWeight.w100,
                                      fontSize: fontSizeArabic),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Divider()
              ],
            ),
          ),
        );
      },
    );
  }

//  Widget _showTranslate(int pageNum) {
//    TranslateRepository translateRepository = new TranslateRepository();
//    return FutureBuilder<List<Ayat>>(
//      future: translateRepository.getPageTranslate(pageNum),
//      builder: (_, snapshot) {
//        if (snapshot.connectionState == ConnectionState.done) {
//          List<Ayat> ayat = snapshot.data;
//          return AlertDialog(
//            contentPadding: EdgeInsets.all(5),
//            content: Container(
//              width: 300.0,
//              height: 500.0,
//              child: ListView.builder(
//                  itemCount: ayat.length,
//                  itemBuilder: (_, position) {
//                    Ayat aya = ayat[position];
//                    return Text(
//                      "${aya.translate} (${aya.ayaNum})",
//                      textAlign: TextAlign.center,
//                    );
//                  }),
//            ),
//          );
//        } else {
//          return Center(
//            child: CircularProgressIndicator(),
//          );
//        }
//      },
//    );
//  }

  Widget _newBookmarkDialog(context, index) {
    SoraBookmark soraBookmark = soraBookmarkList[index];
    _title = soraBookmark.SoraName_ar;
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      backgroundColor: Theme.of(context).primaryColorLight,
      child: Container(
        height: 130,
        decoration: BoxDecoration(
          border:
              Border.all(color: Theme.of(context).hoverColor.withOpacity(0.5)),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  'assets/images/zakh2.png',
                  fit: BoxFit.cover,
                  color: Theme.of(context).hoverColor.withOpacity(0.2),
                )),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  localeHelper.addNewBookmark(),
                  style: TextStyle(
                      color: Theme.of(context).hoverColor,
                      fontFamily: 'cairo',
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Theme.of(context).hoverColor,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        onChanged: (value) {
                          setState(() {
                            _title = value.trim();
                          });
                        },
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColorDark,
                                    width: 2)),
                            hintText: soraBookmark.SoraName_ar,
                            suffixIcon: const Icon(
                              Icons.bookmark,
                              color: Color(0x99f5410a),
                            ),
                            hintStyle: TextStyle(
                                height: 0.8,
                                color: Theme.of(context)
                                    .primaryColorLight
                                    .withOpacity(0.5),
                                fontFamily: 'cairo',
                                fontSize: 15)),
                      )),
                ),
                RaisedButton(
                  color: Theme.of(context).primaryColorDark,
                  child: Text(
                    localeHelper.save(),
                    style: TextStyle(color: Theme.of(context).hoverColor),
                  ),
                  onPressed: () {
                    saveBookmark();
                    Navigator.pop(context);
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }


  Widget _changeWidget(int pageNum){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Builder(
            builder: (context) => InkWell(
              onTap: () {
                BotToast.showAttachedWidget(
                    attachedBuilder: (_) => Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Card(
                        color: Theme.of(context).primaryColorDark,
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Slider(
                                min: SettingsHelpers.minFontSizeArabic,
                                max: maxFontSizeArabic,
                                value: fontSizeArabic,
                                label: '$fontSizeArabic',
                                activeColor:
                                Theme.of(context).hoverColor,
                                inactiveColor: Theme.of(context)
                                    .bottomAppBarColor,
                                onChanged: (double value) async {
                                  SettingsHelpers.instance.fontSizeArabic(value);
                                  setState(
                                        () {
                                      fontSizeArabic = value;
                                    },
                                  );
                                  _myEventBus.eventBus.fire(
                                      FontSizeEvent()..arabicFontSize = value);
                                },
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    color: Theme.of(context)
                                        .hoverColor,
                                    borderRadius:
                                    BorderRadius.only(
                                      topLeft:
                                      Radius.circular(5.0),
                                      topRight:
                                      Radius.circular(5.0),
                                      bottomLeft:
                                      Radius.circular(5.0),
                                      bottomRight:
                                      Radius.circular(5.0),
                                    )),
                                alignment: Alignment.center,
                                child: Padding(
                                  padding:
                                  const EdgeInsets.all(4.0),
                                  child: Text(
                                      '${fontSizeArabic.toInt()}',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Theme.of(context)
                                              .primaryColorLight)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    wrapToastAnimation:
                        (controller, cancel, Widget child) =>
                        CustomAttachedAnimation(
                          controller: controller,
                          child: child,
                        ),
                    animationDuration: Duration(milliseconds: 200),
                    enableSafeArea: true,
                    duration: Duration(seconds: 20),
                    targetContext: context);
              },
              child: FlatButton.icon(
                label: Text('حجم الخط',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'Tajawal'
                ),),
                icon: Icon(Icons.format_size,
                size: 30,
                color: Theme.of(context).primaryColorLight,),
                color: Theme.of(context).primaryColorLight,),
            ),
          ),

          SizedBox(width: 16.0,),
          Builder(
            builder: (context) => InkWell(
              onTap: () {
                BotToast.showAttachedWidget(
                    attachedBuilder: (_) => Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Card(
                        color: Theme.of(context).primaryColorDark,
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              _ayaList(currentPage),
                              _ayaList2(currentPage),
//                              RaisedButton(
//                                onPressed: () {
//                                  setState(() {
//                                    value = _changeTafseer();
//                                  });
//                                  },
//                                child: Text('تفسير القرطبي'),
//                              ),
//                              RaisedButton(
//                                onPressed: (){
//                                  setState(() {
////                                    _tafseer();
//                                  });
//                                },
//                                child: Text('تفسير ابن كثير'),
//                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    wrapToastAnimation:
                        (controller, cancel, Widget child) =>
                        CustomAttachedAnimation(
                          controller: controller,
                          child: child,
                        ),
                    animationDuration: Duration(milliseconds: 200),
                    enableSafeArea: true,
                    duration: Duration(seconds: 20),
                    targetContext: context);
              },
              child: FlatButton.icon(
                label: Text('التفاسير',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontFamily: 'Tajawal'
                  ),),
                icon: Icon(FontAwesome.book,
                  size: 30,
                  color: Theme.of(context).primaryColorLight,),
                color: Theme.of(context).primaryColorLight,),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomAttachedAnimation extends StatefulWidget {
  final AnimationController controller;
  final Widget child;

  const CustomAttachedAnimation({Key key, this.controller, this.child})
      : super(key: key);

  @override
  _CustomAttachedAnimationState createState() =>
      _CustomAttachedAnimationState();
}

class _CustomAttachedAnimationState extends State<CustomAttachedAnimation> {
  Animation<double> animation;
  static final Tween<Offset> tweenOffset = Tween<Offset>(
    begin: const Offset(0, 40),
    end: const Offset(0, 0),
  );

  @override
  void initState() {
    animation =
        CurvedAnimation(parent: widget.controller, curve: Curves.decelerate);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      child: widget.child,
      animation: widget.controller,
      builder: (BuildContext context, Widget child) {
        return ClipRect(
          child: Align(
            heightFactor: animation.value,
            widthFactor: animation.value,
            child: Opacity(
              opacity: animation.value,
              child: child,
            ),
          ),
        );
      },
    );
  }
}
