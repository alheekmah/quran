import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:alquranalkareem/data/model/sorah.dart';
import 'package:alquranalkareem/data/repository/sorah_repository.dart';
import 'package:alquranalkareem/helper/locale_helper.dart';
import 'package:alquranalkareem/module/quran/show.dart';

class SorahList extends StatefulWidget {
  @override
  _SorahListState createState() => _SorahListState();
}

class _SorahListState extends State<SorahList>
    with AutomaticKeepAliveClientMixin<SorahList> {
  SorahRepository sorahRepository = new SorahRepository();
  List<Sorah> sorahList;

  @override
  void initState() {
    super.initState();
    getList();
  }

  getList() async {
   sorahRepository.all().then((values) {
     setState(() {
       sorahList = values;
     });
   });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    LocaleHelper localeHelper = new LocaleHelper(context: context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: false,
        title: Text(
          localeHelper.quranSorah(),
          style: TextStyle(fontFamily: "cairo",
              fontSize: 18,
          fontWeight: FontWeight.normal,
          color: Theme.of(context).hoverColor),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Scrollbar(

              child: sorahList != null ? ListView.builder(
                  physics: const AlwaysScrollableScrollPhysics (),
                  itemCount: sorahList.length,
                  itemBuilder: (_,index) {
                    Sorah sorah = sorahList[index];
                    return Container(
                      color: ( index % 2 == 0 ? Theme.of(context).bottomAppBarColor : Theme.of(context).hoverColor),
                      child: ListTile(
                        onTap: (){
                          Navigator.push(context, new MaterialPageRoute(
                              builder: ((context) => QuranShow(initialPageNum: sorah.pageNum,))
                          ));
                        },
                        leading: Padding(
                          padding: const EdgeInsets.only(bottom: 6),
                          child: CircleAvatar(
                              backgroundImage: AssetImage("assets/images/sora_num.png",),
                              radius: 25,
                              backgroundColor: Colors.transparent,
                              child: Padding(
                                padding: const EdgeInsets.only(top:5.0),
                                child: Text(
                                  "${sorah.id}",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontFamily: "maddina",
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                        ),
                        title: Text(localeHelper.lang() == 'en' ? sorah.nameEn : sorah.name,
                          style: TextStyle(
                              fontFamily: "naskh",
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              color: ( index % 2 == 0 ? Theme.of(context).hoverColor : Theme.of(context).primaryColorDark)),
                        ),
                        subtitle: RichText(
                            text: TextSpan(children: [
                              WidgetSpan(
                                child: Text(
                                  "${sorah.ayaCount}",
                                  style: TextStyle(
                                      fontFamily: "maddina",
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                  color: ( index % 2 == 0 ? Theme.of(context).hoverColor : Theme.of(context).primaryColor)),
                                ),
                              ),
                              WidgetSpan(
                                child: Text(
                                  "${localeHelper.ayaCount()}: ",
                                  style: TextStyle(fontFamily: "uthman",
                                  color: ( index % 2 == 0 ? Theme.of(context).hoverColor : Theme.of(context).primaryColor)),
                                ),
                              ),
                            ])),
                        trailing: Text(
                              '${localeHelper.lang() == 'ar' ? sorah.nameEn : sorah.name}',
                              style: TextStyle(
                                color: Theme.of(context).primaryColorLight
                              ),
                            ),
                      ),
                    );
                  }
              ) : Text(""),
            ),
          )
        ],
      ),
    );

  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
