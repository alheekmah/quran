import 'package:flutter/material.dart';
import 'package:preferences/preference_service.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';

class LocaleHelper {
  BuildContext context;

  LocaleHelper({@required this.context});

  String lang(){
    return PrefService.getString("lang");
  }

  String searchHint() {
    return AppLocalizations.of(context).translate("search_hint");
  }

  String searchWord() {
    return AppLocalizations.of(context).translate("search_word");
  }

  String searchDescription() {
    return AppLocalizations.of(context).translate("search_description");
  }

  String sorah() {
    return AppLocalizations.of(context).translate("sorah");
  }

  String part() {
    return AppLocalizations.of(context).translate("part");
  }

  String menu() {
    return AppLocalizations.of(context).translate("menu");
  }

  String notes() {
    return AppLocalizations.of(context).translate("notes");
  }

  String noteTitle() {
    return AppLocalizations.of(context).translate("note_title");
  }

  String noteDetails() {
    return AppLocalizations.of(context).translate("note_details");
  }

  String addNewNote() {
    return AppLocalizations.of(context).translate("add_new_note");
  }

  String bookmarks() {
    return AppLocalizations.of(context).translate("bookmarks");
  }

  String bookmarkTitle() {
    return AppLocalizations.of(context).translate("bookmark_title");
  }

  String addNewBookmark() {
    return AppLocalizations.of(context).translate("add_new_bookmark");
  }

  String save() {
    return AppLocalizations.of(context).translate("save");
  }

  String edit() {
    return AppLocalizations.of(context).translate("edit");
  }

  String delete() {
    return AppLocalizations.of(context).translate("delete");
  }

  String tafseer() {
    return AppLocalizations.of(context).translate("tafseer");
  }

  String translate() {
    return AppLocalizations.of(context).translate("translate");
  }

  String azkar() {
    return AppLocalizations.of(context).translate("azkar");
  }

  String qibla() {
    return AppLocalizations.of(context).translate("qibla");
  }

  String salat() {
    return AppLocalizations.of(context).translate("salat");
  }

  String ryad() {
    return AppLocalizations.of(context).translate("ryad");
  }

  String ayaCount() {
    return AppLocalizations.of(context).translate("aya_count");
  }

  String quranSorah() {
    return AppLocalizations.of(context).translate("quran_sorah");
  }

  String aboutUs() {
    return AppLocalizations.of(context).translate("about_us");
  }

  String stopTitle() {
    return AppLocalizations.of(context).translate("stop_title");
  }

  String aboutApp() {
    return AppLocalizations.of(context).translate("about_app");
  }

  String email() {
    return AppLocalizations.of(context).translate("email");
  }

  String selectPlayer() {
    return AppLocalizations.of(context).translate("select_player");
  }

  String page() {
    return AppLocalizations.of(context).translate("page");
  }

  String fontSize() {
    return AppLocalizations.of(context).translate("fontSize");
  }

  String waqfName() {
    return AppLocalizations.of(context).translate("waqfName");
  }

  String onboardTitle1() {
    return AppLocalizations.of(context).translate("onboardTitle1");
  }

  String onboardDesc1() {
    return AppLocalizations.of(context).translate("onboardDesc1");
  }

  String onboardTitle2() {
    return AppLocalizations.of(context).translate("onboardTitle2");
  }

  String onboardDesc2() {
    return AppLocalizations.of(context).translate("onboardDesc2");
  }

  String onboardTitle3() {
    return AppLocalizations.of(context).translate("onboardTitle3");
  }

  String onboardDesc3() {
    return AppLocalizations.of(context).translate("onboardDesc3");
  }

  String light() {
    return AppLocalizations.of(context).translate("light");
  }

  String dark() {
    return AppLocalizations.of(context).translate("dark");
  }

  String azkarfav() {
    return AppLocalizations.of(context).translate("azkarfav");
  }

  String themeTitle() {
    return AppLocalizations.of(context).translate("themeTitle");
  }

  String aboutApp2() {
    return AppLocalizations.of(context).translate("about_app2");
  }

  String aboutApp3() {
    return AppLocalizations.of(context).translate("about_app3");
  }

}